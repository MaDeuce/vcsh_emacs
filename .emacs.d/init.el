;;; package --- Main init file
;;; Commentary:
;;;
;;; With apologies to William H. Rupertus, Major General USMC.
;;;
;;; This is my emacs init.  There are many like it, but this one is mine.
;;; My emacs init is my best friend.  It is my life.  I must master it as I must
;;; master my life...

;;; Code:

(add-to-list 'load-path (concat user-emacs-directory "elisp"))

(require 'base)
(require 'base-display)
(require 'base-theme)
(require 'base-macos)
(require 'base-extensions)
(require 'base-nikola)
(require 'lang-python)

(message "init.el completed")
