;; (use-package anaconda-mode
;;   :bind ("C-M-i" . anaconda-mode-complete) 
;;   :config (progn
;;          (add-hook 'python-mode-hook 'python-settings)
;;          (add-hook 'python-mode-hook 'anaconda-mode)
;;          (add-hook 'python-mode-hook 'anaconda-eldoc-mode))
;;  :ensure t)

(defun python-settings ()
  (turn-on-auto-fill)
  (set-fill-column 78)
  ;; PEP8 compliance stuff
  (setq indent-tabs-mode nil) ;; only use spaces for indentation
  (setq tab-width 4)
  (setq python-indent-offset 4)
  (setq tab-stop-list (number-sequence 4 120 4)))

(defun disable-pythonstartup ()
  ;;; In my environment, PYTHONSTARTUP="~/.pystartup".  My .pystartup
  ;;; sets up command history, readline, and runs the 'ptpython' REPL
  ;;; instead of the standard REPL.  emacs does not like this.  This
  ;;; function tells python to ignore environment variables when starting,
  ;;; thereby running the standard REPL.
  (setq python-shell-interpreter-args "-E"))

;;;
;;; flycheck displays lint/syntax errors in the buffer being edited
;;;     flake8 is flycheck's default python syntax checker.
;;;     Be sure flake8 is present in the curren venv.
;;;     Install flake8 via 'pip install flake8'.
(use-package flycheck
  :config (add-hook 'python-mode-hook 'flycheck-mode)
  :ensure t)

(defun enable-flycheck-pycheckers ()
  ;;; multiple syntax checker for Python using flycheck
  ;;; https://github.com/msherry/flycheck-pycheckers
  ;;; uncomment next line to enable flycheck in all buffers
  (global-flycheck-mode 1)
  (with-eval-after-load 'flycheck
    (add-hook 'flycheck-mode-hook #'flycheck-pycheckers-setup)))
(enable-flycheck-pycheckers)

(use-package elpy
  :config (progn
            (add-hook 'python-mode-hook 'python-settings)
            (when (require 'flycheck nil t)
              ;; if flycheck present, disable flymake, enable flycheck
              (setq elpy-modules (delq 'elpy-module-flymake elpy-modules))
              (add-hook 'elpy-mode-hook 'flycheck-mode))
            (add-hook 'elpy-mode-hook 'company-mode)
            (disable-pythonstartup)
            (elpy-enable))
  :ensure t)

;;; Requires that isort be installed via 'pip install isort'.
;;; Provides:  
;;;     M-x py-isort-buffer RET
;;;     M-x py-isort-region RET
;;; If you want py-isort to process the current buffer before saving,
;;; uncomment the :config line.
(use-package py-isort
  ;;; :config (add-hook 'before-save-hook 'py-isort-before-save)
  :ensure t)

;;; Force elpy to use python3 for its "RPC Python".  It should be using
;;; whatever the virtualenv is, but it apparently isn't.
;;; https://github.com/jorgenschaefer/elpy/issues/1001
;;; This seems to resolve the issue.
(setq elpy-rpc-python-command "python3")

(provide 'lang-python)
(message "loaded: lang-python.el")
;;; end: lang-python.el
