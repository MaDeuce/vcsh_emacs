;;; Config for the 'nikola' static blog package.
;;; Only needed if nikola is configured to process org-mode files as input.
;;;

(use-package htmlize :ensure t)

(provide 'base-nikola)
(message "loaded: base-nikola.el")
