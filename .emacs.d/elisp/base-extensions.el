;;; Highlight character at fill-column position
;;; M-x fci-mode
;;; Color names: M-x list-colors-display
(use-package fill-column-indicator
  :config (progn
            ;; Config on graphics displays
            (setq fci-rule-width 1)
            (setq fci-rule-color "grey50")

            ;; Config on character terminals
            ;; (setq fci-rule-character 'x)
            (setq fci-rule-character-color "grey50")

            ;; Use characters everywhere, even if graphics terminal
            ;; (setq fci-always-se-textual-rule 't)

            (add-hook 'python-mode-hook 'fci-mode))
  :ensure t)

(use-package markdown-mode
  :config (progn
            ;;; ~/khe/bin/markd -> opens "Markd 2" viewer
            (setq markdown-open-command "markd")
            (add-to-list 'auto-mode-alist '("\\.pmd$" . markdown-mode))
            (add-to-list 'auto-mode-alist '("\\.md$"  . markdown-mode)))
  :ensure t)

(use-package rst 
  :config (progn
            (add-to-list 'auto-mode-alist '("\\.txt$"  . rst-mode))
            (add-to-list 'auto-mode-alist '("\\.rst$"  . rst-mode))
            (add-to-list 'auto-mode-alist '("\\.rest$" . rst-mode)))
  :ensure t)

(use-package yaml-mode
  :config (progn
            (add-to-list 'auto-mode-alist '("\\.yaml$"  . yaml-mode))
            (add-to-list 'auto-mode-alist '("\\.yml$"   . yaml-mode)))
  :ensure t)

(use-package json-mode
  :config (progn
            (add-to-list 'auto-mode-alist '("\\.json$"    . json-mode))
            (add-to-list 'auto-mode-alist '("\\.json5$"   . json-mode)))
            (add-to-list 'auto-mode-alist '("\\.hjson$"   . json-mode))
  :ensure t)

(use-package which-key
  :config (which-key-mode))

;; Provides commands to get/put github gists.
(use-package gist :ensure t)

;; Reinstate Zippy
(require 'yow)
(setq yow-file "~/.emacs.d/etc/yow.lines")

;; Support for buffer tabs
;; This is actually pretty horrible - it slows everything to a crawl.
;; (use-package awesome-tab
;;   :load-path "~/.emacs.d/elisp/awesome-tab.el"
;;   :config
;;   (awesome-tab-mode t)
;;)

(provide 'base-extensions)
(message "loaded: base-extensions.el")
;;; end: base-extensions.el
