(package-initialize)
(add-to-list 'package-archives
             '("melpa" . "https://melpa.org/packages/"))

(when (not package-archive-contents)
  (package-refresh-contents))

(unless (package-installed-p 'use-package)
  (package-install 'use-package))
    
(defconst private-dir  (expand-file-name "private" user-emacs-directory))
(defconst temp-dir (format "%s/cache" private-dir) "Hostname-based elisp temp directories")

;; UTF-8 FTW
(set-charset-priority        'unicode)
(setq locale-coding-system   'utf-8)   ; pretty
(set-terminal-coding-system  'utf-8)   ; pretty
(set-keyboard-coding-system  'utf-8)   ; pretty
(set-selection-coding-system 'utf-8)   ; please
(prefer-coding-system        'utf-8)   ; with sugar on top
(setq default-process-coding-system '(utf-8-unix . utf-8-unix))

;;; Tell emacs to save customizations, if any, to a file
;;; other than ~/.emacs (the default).  This makes it easy to try out
;;; customizations without making 'dotfiles' think something important
;;; has changed.
(defconst custom-file (expand-file-name "emacs-custom.el" user-emacs-directory))

;;; Create empty custom-file, if it doesn't exist.  Avoids
;;; errors when load is subsequently called.
(unless (file-exists-p custom-file) (write-region "" "" custom-file))
(load custom-file)

;;; Directory that autosave filles will be placed in.  Autosaves will fail
;;; if directory does not exist; create it, if necessary.
(setq auto-save-dir (concat temp-dir "/autosave-files/"))
(unless (file-directory-p auto-save-dir) (make-directory auto-save-dir :parents))

;;; Configure backups
(setq 
 backup-directory-alist            `((".*" . ,(concat temp-dir "/backup/")))
 auto-save-file-name-transforms    `((".*"   ,auto-save-dir t))
 auto-save-default                 t
 auto-save-list-file-name          (concat temp-dir "/autosave-file-list")
 backup-by-copying                 t  ; Don't delink hardlinks
 version-control                   t  ; Use version numbers on backups
 delete-old-versions               t  ; Automatically delete excess backups
 kept-new-versions                20  ; how many of the newest versions to keep
 kept-old-versions                 5  ; and how many of the old
 )

;; Misc. customizations
(setq 
 ;; The bell is annoying with scroll overrun at buffer top/bottom.
 visible-bell                        nil
 ring-bell-function                  'ignore
 ;; http://ergoemacs.org/emacs/emacs_stop_cursor_enter_prompt.html
 minibuffer-prompt-properties
 '(read-only t point-entered minibuffer-avoid-prompt face minibuffer-prompt)
 ;; Disable non selected window highlight
 cursor-in-non-selected-windows     nil
 highlight-nonselected-windows      nil
 inhibit-startup-message            t
 use-package-always-ensure          t)

;;; Enable upcase and downcase region
(put 'downcase-region 'disabled nil)
(put 'upcase-region   'disabled nil)

;;; Make all 'yes or no' prompts show 'y or n'
(fset 'yes-or-no-p 'y-or-n-p)

(defun org-mode-setup ()
  (require 'org)
  (define-key global-map "\C-cl" 'org-store-link)
  (define-key global-map "\C-ca" 'org-agenda)

  ;; Configure supported languages for literate programming.
  ;; https://orgmode.org/worg/org-contrib/babel/intro.html#getting-started
  ;; http://howardism.org/Technical/Emacs/literate-devops.html
  (org-babel-do-load-languages
   'org-babel-load-languages
   '((awk . t)     ;; https://orgmode.org/worg/org-contrib/babel/languages/ob-doc-awk.html
     (dot . t)     ;; https://orgmode.org/worg/org-contrib/babel/languages/ob-doc-dot.html
     (python . t)  ;; https://orgmode.org/worg/org-contrib/babel/languages/ob-doc-python.html
     (shell . t)
     (sed . t)))

  (add-hook 'org-mode-hook
            (lambda () (face-remap-add-relative 'default :family "Monaco")))

  (setq org-log-done t))
(org-mode-setup)

(defun outline-magic-setup ()
  ;; This package provides vibility cycling similar to that of org mode.
  ;; Installed for use with markdown-mode, but can be used elsewhere.
  ;; Manually install outline-magic.el in ~/.elisp.d/lisp and byte-compile.
  ;; https://staff.fnwi.uva.nl/c.dominik/Tools/outline-magic.el
  (declare-function 'outline-cycle "outline-magic")
  (add-hook 'outline-mode-hook 
            (lambda () 
              (require 'outline-cycle)))
  (add-hook 'outline-minor-mode-hook 
            (lambda () 
              (require 'outline-magic)
              (define-key outline-minor-mode-map [(f9)] 'outline-cycle)))
  )
;; (outline-magic-setup)

(defun modify-zap ()
  ;; Replace zap-to-char with zap-up-to-char
  (autoload 'zap-up-to-char "misc"
    "Kill up to, but not including ARGth occurrence of CHAR.
  
   \(fn arg char)"
    'interactive)

  (global-set-key "\M-z" 'zap-up-to-char))
(modify-zap)
;;; Use fastest available method to display line numbers
(if (version<= "26.0.5" emacs-version)
    (global-display-line-numbers-mode)
  (linum-mode))

;;; Display line and column numbers in mode line.
(setq line-number-mode t)
(setq column-number-mode t)

;;; It is worth mentioning here that the key sequence ESC 9 9 9 9 9 9 9 9 ^X f
;;; sets fill-column to a very large number. With that, the key sequence M-q ,
;;; or M-x fill-paragraph, unfills paragraphs. (That is eight digit 9s. Going
;;; to nine of them wraps the number to -73741825 and foils that behavior.) 

;;; Stefan Monnier <foo at acm.org>. It is the opposite of fill-paragraph    
(defun unfill-paragraph (&optional region)
  "Takes a multi-line paragraph and makes it into a single line of text."
  (interactive (progn (barf-if-buffer-read-only) '(t)))
  (let ((fill-column (point-max))
        ;; This would override `fill-column' if it's an integer.
        (emacs-lisp-docstring-fill-column t))
    (fill-paragraph nil region)))

(define-key global-map "\M-Q" 'unfill-paragraph)

(defun unfill-region (beg end)
  "Unfill the region, joining text paragraphs into a single
    logical line.  This is useful, e.g., for use with
    `visual-line-mode'."
  (interactive "*r")
  (let ((fill-column (point-max)))
    (fill-region beg end)))

;;; Creates an untabify minor mode.  If enabled, the buffer is
;;; untabified prior to save.
(defvar untabify-this-buffer)
(defun untabify-all ()
  "Untabify the current buffer, unless `untabify-this-buffer' is nil."
  (and untabify-this-buffer (untabify (point-min) (point-max))))
(define-minor-mode untabify-mode
  "Untabify buffer on save." nil " untab" nil
  (make-variable-buffer-local 'untabify-this-buffer)
  (setq untabify-this-buffer (not (derived-mode-p 'makefile-mode)))
  (add-hook 'before-save-hook #'untabify-all))
(add-hook 'prog-mode-hook 'untabify-mode)

(add-hook 'text-mode-hook 'auto-fill-mode)
(add-hook 'text-mode-hook (lambda () (set-fill-column 88)))

(add-hook 'LaTeX-mode-hook 'flyspell-mode)
(add-hook 'LaTeX-mode-hook 'flyspell-buffer)

(provide 'base)
(message "loaded: base.el")
;;; base.el ends here
