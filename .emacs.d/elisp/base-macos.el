(defun os-x-setup ()
  ;; alternative: 1 line at a time, progressive speed t, but scrolling buffers
  (setq mouse-wheel-scroll-amount '(5 ((shift) . 1)))  ; five lines at a time
  (setq mouse-wheel-progressive-speed nil)             ; don't accel scrolling
)

(defun os-x-setup2 ()
  (setq mouse-wheel-scroll-amount '(0.025))
  (setq mouse-wheel-progressive-speed nil)
)
(os-x-setup2)

;;; 10/4/2018 - seems to hose anaconda completion
;;; OS-X 
;;; mouse settings - enable 'secondary click' on right side of magic mouse
;;; Right click on mispelled work will display spellings to choose from.
(eval-after-load "flyspell"
    '(progn
       (define-key flyspell-mouse-map [down-mouse-3] #'flyspell-correct-word)
       (define-key flyspell-mouse-map [mouse-3] #'undefined)))

;;; OS-X doesn't have ispell
;;; Use ports to install aspell
(setq ispell-program-name "aspell")
(add-to-list 'exec-path' "/opt/local/bin")

;;; On macOS $PATH/exec-path for gui-based emacs does not match login $PATH.
;;; When on macOS, this sets exec-path correctly from a subshell.
;; (use-package exec-path-from-shell
;;   :config (when (memq window-system '(mac ns))
;; 	    (exec-path-from-shell-initialize))
;;   :ensure t)

;;; Enable three button mouse on OS-X
;;;
;;;    button 2 = <option> click
;;;    button 3 = <command> click
;;;
(setq mac-emulate-three-button-mouse t)

(provide 'base-macos)
(message "loaded: base-macos.el")
;;; base-macos.el ends here
