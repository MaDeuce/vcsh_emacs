;;; Set frame sizes for graphic frames when using emacs --daemon and emacsclient.
;;; Based on: https://www.emacswiki.org/emacs/SettingFrameColorsForEmacsClient
;;;
;;; Set width and height as desired in function below.
;;;

(defun setup-window-system-frame-size (&rest frame)
  (if window-system
      (let ((height 60)
	    (width 100)
	    (f (if (car frame)
		   (car frame)
		 (selected-frame))))
	(progn
	  (set-frame-size f width height)))))

(require 'server)
(defadvice server-create-window-system-frame
  (after set-window-system-frame-size ())
  "Set custom frame size on graphics display."
  (message "base-display.el: changing frame size after server-create-window-system-frame")
  (setup-window-system-frame-size))
(ad-activate 'server-create-window-system-frame)
(add-hook 'after-make-frame-functions 'setup-window-system-frame-size t)

;; ;; Original approach from: https://stackoverflow.com/questions/5795451
;; ;;
;; ;; Specify frame sizes for graphic and character frames:
;; (setq graphic-height  60)
;; (setq graphic-width  100)
;; (setq text-height     24)
;; (setq text-width      80)
;;
;; (defun new-frame-size (hook-name mode height width)
;;   "Set new frame size."
;;   (let 
;;       ((frame-alist (list (cons 'width width) (cons 'height height))))
;;     (setq initial-frame-alist frame-alist)
;;     (setq default-frame-alist frame-alist))
;;   (message "set-frame-size: hook=%s, mode=%s, %dx%d" hook-name mode height width))
;;
;; (defun new-frame-size-by-type (which-hook)
;;   "Set frame size based on type of frame."
;;   (if (display-graphic-p)
;;       (new-frame-size which-hook "graphic" graphic-height graphic-width)
;;     (new-frame-size which-hook "non-graphic" text-height text-width)))
;;
;; (add-hook 'after-init-hook            (lambda ()          (new-frame-size-by-type "after-init-hook")))
;; (add-hook 'after-make-frame-functions (lambda (&rest foo) (new-frame-size-by-type "after-make-frame-functions")))

(provide 'base-display)
(message "loaded: base-display.el")
